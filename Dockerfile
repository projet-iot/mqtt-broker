# PORT 1883 pour broker mosquitto
FROM alpine

RUN apk update
RUN apk add mosquitto

COPY mosquitto.conf /etc/mosquitto/mosquitto.conf

ENTRYPOINT ["/usr/sbin/mosquitto", "-c", "/etc/mosquitto/mosquitto.conf"]
